#!/bin/bash
# Welcome to the progressionlive laptop script!
# Be prepared to turn your laptop (or desktop, no haters here)
# into an awesome development machine.
fancy_echo() {
  local fmt="$1"; shift
  # shellcheck disable=SC2059
  printf "\\n$fmt\\n" "$@"
}

trap 'ret=$?; test $ret -ne 0 && printf "failed\n\n" >&2; exit $ret' EXIT

read -s -p "Enter Password for sudo: " sudopw

if ! command -v snap >/dev/null; then
    fancy_echo "Installing snap ..."
    echo "$sudopw" | sudo apt update
    echo "$sudopw" | sudo apt install snapd -yqq

    export PATH="/usr/local/bin:$PATH"
    fancy_echo "Installing snap deamon..."
    echo "$sudopw" | sudo snap install snapd
else
  fancy_echo "Snap already installed. Skipping ..."
fi

if ! command -v git >/dev/null; then
    fancy_echo "Installing git ..."
    echo "$sudopw" | sudo apt-get update
    echo "$sudopw" | sudo apt-get install git -yqq
else
  fancy_echo "Git already installed. Skipping ..."
fi
if [ ! -d "$HOME/.bin/" ]; then
  mkdir "$HOME/.bin"
fi

fancy_echo "Installing vscode..."
echo "$sudopw" | sudo snap install --classic code

if ! command -v jetbrains-toolbox >/dev/null; then
  fancy_echo "Installing jetbrains-toolbox..."
  curl https://raw.githubusercontent.com/nagygergo/jetbrains-toolbox-install/master/jetbrains-toolbox.sh | bash
else
  fancy_echo "jetbrains-toolbox already installed. Skipping ..."
fi

fancy_echo "Installing intellij-idea-ultimate..."
echo "$sudopw" | sudo snap install intellij-idea-ultimate --classic

fancy_echo "Installing android-studio..."
echo "$sudopw" | sudo snap install android-studio --classic

fancy_echo "Installing datagrip..."
echo "$sudopw" | sudo snap install datagrip --classic

fancy_echo "Installing postman..."
echo "$sudopw" | sudo snap install postman

if ! command -v docker >/dev/null; then
  fancy_echo "Installing docker..."
  curl -fsSL https://get.docker.com -o get-docker.sh
  echo "$sudopw" | sudo sh get-docker.sh

  fancy_echo "Adding docker group..."
  echo "$sudopw" | sudo groupadd docker

  fancy_echo "Adding your user to the group..."
  echo "$sudopw" | sudo usermod -aG docker $USER
else
  fancy_echo "docker installed. Skipping ..."
fi

fancy_echo "Installing slack..."
echo "$sudopw" | sudo snap install slack --classic

fancy_echo "Installing firefox..."
echo "$sudopw" | sudo snap install firefox

fancy_echo "Installing libreoffice..."
echo "$sudopw" | sudo snap install libreoffice

fancy_echo "Installing ngrok..."
echo "$sudopw" | sudo snap install ngrok

fancy_echo "Installing tmux..."
echo "$sudopw" | sudo snap install tmux --classic

fancy_echo "Installing  aws-cli..."
echo "$sudopw" | sudo snap install aws-cli --classic

fancy_echo "Installing openjdk-8-jdk..."
## JAVA
echo "$sudopw" | sudo apt-get update
echo "$sudopw" | sudo apt-get install openjdk-8-jdk -yqq

## no need for sudo password now..
unset sudopw

fancy_echo "Setuping dev directory..."

fancy_echo "changing to user home directory..."
cd /home/"$USER"

if [ ! -d "dev" ]; then
  fancy_echo "dev directory doesn't exist...creating it"
  mkdir dev
fi

fancy_echo "changing to dev directory..."
cd dev

git clone https://bitbucket.org/progressionlive/progression.git
git clone https://bitbucket.org/progressionlive/progression-custom.git
git clone https://bitbucket.org/progressionlive/progression-android.git

fancy_echo "Don't forget to configure your email, user and ssh key for git..."

fancy_echo "To verify your docker installation please run docker run hello-world"